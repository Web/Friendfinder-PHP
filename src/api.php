<?php
namespace Api\V1;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

function GetMessage (Application $app, $pubkey) {
    // Decode public key
    $pubkey = Crypt\base64url_decode($pubkey);

    // Check public key
    if(strlen($pubkey) != 32)
        return $app->json(['message' => 'Invalid public key.'], 400);

    // Load message from disk
    $json = @file_get_contents($app['data_path'] . Crypt\base64url_encode($pubkey));

    // Show error if not found
    if ($json === false)
        return $app->json(['message' => 'No message for public key.'], 404);

    // Return message
    return JsonResponse::fromJsonString($json);
}

function PutMessage (Application $app, Request $request, $pubkey = null) {
    // Decode message
    $data = @Crypt\decode($request->getContent());
    if ($data === false)
        return $app->json(['message' => 'The message could not be decoded.'], 400);

    // Set public key
    if($pubkey === null)
        $pubkey = $data->pubkey;
    else
        $pubkey = Crypt\base64url_decode($pubkey);

    // Check public key
    if(strlen($pubkey) != 32)
        return $app->json(['message' => 'Invalid public key.'], 400);

    // Load existing message
    $json = @file_get_contents($app['data_path'] . Crypt\base64url_encode($pubkey));

    // Check if submitted message is newer
    if($json && $data->time < json_decode($json)->time)
        return $app->json(['message' => 'The submitted message is older than the existing message.'], 400);

    // Verify message
    try {
        $verification = Crypt\verify_data($data, $pubkey);
    } catch (Error $e) {
        return $app->json(['message' => 'The message could not be verified.'], 400);
    }

    if ($verification === false)
        return $app->json(['message' => 'The message failed validation.'], 400);

    // Check if data directory exists or create it
    if (!is_dir($app['data_path']))
        mkdir($app['data_path']);

    // Store message to disk and return success
    if(@file_put_contents($app['data_path'] . Crypt\base64url_encode($pubkey), $request->getContent()))
        return $app->json(['message' => 'Success']);

    // Return error if something went wrong
    return $app->json(['message' => 'Something went wrong'], 500);
}
