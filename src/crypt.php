<?php
namespace Api\V1\Crypt;

function base64url_encode($data) {
    return strtr(base64_encode($data), '+/', '-_');
}

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}


/* Create an array with location information */
function info_array($room = '', $geo = '', $event = '', $comment = '') {
    $loc = [
        'time'    => time(),
        'room'    => $room,
        'geo'     => $geo,
        'url'     => $event,
        'comment' => $comment
    ];
    return json_encode($loc);
}

/* Decode a JSON string to a binary data object */
function decode($json_data) {
    $prop = ['time', 'pubkey', 'signature', 'keys', 'location', 'nonce'];
    $data = json_decode($json_data);

    if($data === null)
        return false;

    foreach($prop as $p)
        if(!property_exists($data, $p))
            return false;

    if(!is_int($data->time))
        return false;

    foreach($data as $key => &$value)
        if(is_array($value))
            foreach($value as &$v)
                $v = base64_decode($v);
        elseif($key != 'time')
            $value = base64_decode($value);

    return $data;
}

/* Encode a binary data object to  a JSON string */
function encode($data) {
    foreach($data as $key => &$value)
        if(is_array($value))
            foreach($value as &$v)
                $v = base64_encode($v);
        elseif($key != 'time')
            $value = base64_encode($value);

    return json_encode($data);
}

/* Encrypt a location and create a data object */
function encrypt($location_json, $public_key, $secret_key, $friend_keys) {
    /* Encrypt location object symmetrically */
    $loc_key   = \Sodium\randombytes_buf(\Sodium\CRYPTO_STREAM_KEYBYTES);
    $loc_nonce = \Sodium\randombytes_buf(\Sodium\CRYPTO_STREAM_NONCEBYTES);
    $location  = \Sodium\crypto_stream_xor($location_json, $loc_nonce, $loc_key);

    /* Generate nonce for asymmetric encryption */
    $nonce = \Sodium\randombytes_buf(\Sodium\CRYPTO_BOX_NONCEBYTES);

    /* Convert secret key to curve25519 */
    $secret_box_key = \Sodium\crypto_sign_ed25519_sk_to_curve25519($secret_key);

    /* Encrypt location nonce and key for each friend */
    $keys = [];
    foreach ($friend_keys as $key) {
        /* Convert friend key to curve25519 */
        $box_key = \Sodium\crypto_sign_ed25519_pk_to_curve25519($key);

        /* Generate keypair */
        $keypair = \Sodium\crypto_box_keypair_from_secretkey_and_publickey($secret_box_key, $box_key);

        /* Encrypt */
        $keys[] = \Sodium\crypto_box($loc_nonce . $loc_key, $nonce, $keypair);
    }

    /* Sign the nonce, keys and location */
    $time      = time();
    $string    = $time . $nonce . implode('', $keys) . $location;
    $signature = \Sodium\crypto_sign_detached($string, $secret_key);

    /* Return JSON object */
    return encode([
        'time'      => $time,
        'pubkey'    => $public_key,
        'signature' => $signature,
        'keys'      => $keys,
        'nonce'     => $nonce,
        'location'  => $location,
    ]);
}

/* Verify the signature in a data object */
function verify($json_data, $sender_public_key = null) {
    $data  = decode($json_data);

    if($data === false)
        return false;

    return verify_data($data, $sender_public_key);
}

/* Verify a decoded data object */
function verify_data($data, $sender_public_key = null) {
    if(is_null($sender_public_key))
        $sender_public_key = $data->pubkey;

    $string = $data->time . $data->nonce . implode('', $data->keys) . $data->location;
    return \Sodium\crypto_sign_verify_detached($data->signature, $string, $sender_public_key);
}

/* Verify and decrypt the location in a data object using a secret key and the sender's public key */
function decrypt($json_data, $secret_key, $sender_public_key = null) {
    $data = decode($json_data);

    if($data === false)
        return false;

    return decrypt_data($data, $secret_key, $sender_public_key);
}

/* Verify and decrypt a decoded data object */
function decrypt_data($data, $secret_key, $sender_public_key = null) {
    /* Set public key from data if not given */
    if(is_null($sender_public_key))
        $sender_public_key = $data->pubkey;

    /* Verify sender signature */
    if (!verify_data($data, $sender_public_key))
        return false;

    /* Convert keys to curve25519 */
    $secret_box_key = \Sodium\crypto_sign_ed25519_sk_to_curve25519($secret_key);
    $sender_box_key = \Sodium\crypto_sign_ed25519_pk_to_curve25519($sender_public_key);

    /* Get the secret key */
    $keypair = \Sodium\crypto_box_keypair_from_secretkey_and_publickey($secret_box_key, $sender_box_key);
    $secret  = false;
    foreach($data->keys as $k) {
        $secret = \Sodium\crypto_box_open($k, $data->nonce, $keypair);
        if ($secret !== false)
            break;
    }

    /* Check if a valid key was returned */
    if ($secret === false)
        return false;

    /* Decrypt and parse location */
    return json_decode(\Sodium\crypto_stream_xor(
        $data->location,
        substr($secret, 0, \Sodium\CRYPTO_STREAM_NONCEBYTES),
        substr($secret, \Sodium\CRYPTO_STREAM_NONCEBYTES)
    ));
}

/* Show the fingerprint for a public key */
function fingerprint($public_key) {
    $hash = \Sodium\crypto_generichash($public_key, null, 16);
    $hex  = \Sodium\bin2hex($hash);
    return implode(':', str_split($hex, 4));
}
