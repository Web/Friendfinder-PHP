# Friendfinder API
This is the server-side for the *FOSDEM Friendfinder*.

## API Specification
There is only one endpoint: `/api/v1/message/:pubkey`,
where `:pubkey` is the base64 encoded Ed25519 public key of the sender.
This endpoint responds to the `GET` and `PUT` methods.
`:pubkey` is optional when using `PUT`.
The data object is a simple JSON array, with the following fields:

| Name      | Type            | Description
|-----------|-----------------|-------------
| time      | `number`        | Unix timestamp of the message.
| pubkey    | `string`        | The Ed25519 public key of the sender. This key is converted to a Curve25519 key for encryption.
| signature | `string`        | Signature for the location object. This is checked by the server and recipient.
| keys      | `array<string>` | Array containing the asymmetrically encrypted nonce and key for the decryption of location.
| nonce     | `string`        | Nonce used for asymmetric decryption.
| location  | `string`        | A symmetrically encrypted (using XSalsa20) location object.

All values are base64 encoded.
The signature signes the concatenation of the nonce, keys and location.

When decrypted, the location object contains the following fields:

| Name      | Type      | Description
|-----------|-----------|-------------
| time      | `number`  | Unix timestamp of the message.
| room      | `string`  | Room name/number.
| geo       | `string`  | Geographic coordinate using decimal notation, can be empty.
| url       | `string`  | URL to a location related event.
| comment   | `string`  | Optional comment.

Example object:

```json
{
    "pubkey": "N0sQNlH2uMHJTokqgdEvuzTZxplQyYyW6I4c7wh6Mo0=",
    "signature": "k5GQAJnaNt5Mu0AREjfFDgMlfM5vjhnpqq7aqdsom+Irp3VqW\/KJ8C7OmmB1gnQkTIS9BkF+C72n9YZVyj87Cg==",
    "keys": [
        "SoI2C4mTEYIpki\/Hd26DDU5bIPWEZHhvshC1n1h1QakqjUmrsv+Y6DVHi8\/8oJ\/bnAIYwVyN9GvJPAGLG1Lzu8ddEn\/4OMzk",
        "fsNpjuZefag\/wpUFuoXrTTNnd22EwaESUb+62aMeXU6DhpZeg+qZt7eSDLP78alka5fAFAGXSR\/Czir4Gh3ZUFOPSF+grCKF",
        "DfjblMMLuGjZVwroQhTKeAl6RMB8Vl7c7Qj7hPe2fPuvnXuliyKwuSEqtUkBdKEjyaJnoiphaVQAVu4ZbOhxgXcnQkiDp5x+",
        "MAAoI\/qyS9xlsld4J++Cllp150qkTsMWqFcIqrHcNPW39c2EqZXY6mDdYmNEq+wBDySjEw5nNr952OtSLsGbqM99yB8tE9sp",
        "p5d5LiK\/AgJpd4XcBYsZ\/tLUIcqx9NbqpZsnSsNr1TiV36QCEXHh0TEwAt88QURJQZahNMuLp7MZEJ023OahqA+2IsAVJcNQ",
        "PMrvVCSv\/5TnOBMfm6CsMTKWOv2xg+p9sfzm96NGhmekAZJKMpxFIecXsWtAuIYaqWxfRYMA+BjhYcEd5WPMAx80hKqJAQF9"
    ],
    "nonce": "YzLWu7qHiNageS3RWqJyjPprPnmIBk05",
    "location": "Zj2oXOp9UAuT1zd3WcwF0Oh\/AmvJCAELvxIcOngx45pfmzuQy+vwvvCh1nv30\/716JCf1ktWF4mEUojAGWVDwZ+8+psKU9tr+LCEvmJDUhyQPN4FO0nNkoXiTjNOX+7eqBgqXhsdnzfSBMOikcKyZorgzUfR800EkeTP9bSKLxUT3cgqrXhqConRczw\/2fhX0SeYkj2ClP5bOqrZ4mcpF7z5HqML7bBNDe7RcY8="
}
```

Example of location data:

```json
{
    "time": 1489163218,
    "room": "H.2215",
    "geo": "050.811667, 0004.381111",
    "url": "https:\/\/fosdem.org\/2017\/schedule\/event\/keynotes_welcome\/",
    "comment": "Walking to that talk!"
}
```
