<pre>
<?php
require_once '../src/crypt.php';
use \Api\V1\Crypt as Crypt;

/* Load or generate our own keypair */
if (file_exists(__DIR__ . '/ed25519')) {
    $key_sec = file_get_contents(__DIR__ . '/ed25519');
    $key_pub = file_get_contents(__DIR__ . '/ed25519.pub');
} else {
    $key = \Sodium\crypto_sign_keypair();
    $key_pub = \Sodium\crypto_sign_publickey($key);
    $key_sec = \Sodium\crypto_sign_secretkey($key);
    file_put_contents(__DIR__ . '/ed25519', $key_sec);
    file_put_contents(__DIR__ . '/ed25519.pub', $key_pub);
}

/* I'm going to send a message to myself */
$sender_pub = $key_pub;

/* Generate random keys for 'friends' and add own key) */
$friend_keys = [$key_pub];
for ($i = 0; $i < 5; $i++)
    $friend_keys[] = \Sodium\crypto_sign_publickey(\Sodium\crypto_sign_keypair());

/* Location data */
$loc = \Api\V1\Crypt\info_array(
    'H.2215',
    '050.811667, 0004.381111',
    'https://fosdem.org/2017/schedule/event/keynotes_welcome/',
    'Walking to that talk!'
);
/* Create packet */
$data = Crypt\encrypt($loc, $key_pub, $key_sec, $friend_keys);

/* Show output */
echo 'My fingerprint: ' . Crypt\fingerprint($key_pub) . "\n";
echo 'Endpoint: /api/v1/message/' . Crypt\base64url_encode($key_pub) . "\n";
echo "Packet: \n";
echo json_encode(json_decode($data), JSON_PRETTY_PRINT) . "\n";

echo 'Valid signature: ' . (Crypt\verify($data) ? 'yes' : 'no') . "\n";
echo "Decrypted location: \n";
echo json_encode(Crypt\decrypt($data, $key_sec, $sender_pub), JSON_PRETTY_PRINT) . "\n\n";

/* Size preview */
echo "Data size:\n";
$data = Crypt\decode($data);
foreach($data as &$value)
    if(is_array($value))
        foreach($value as &$v)
            $v = strlen($v) . ' bytes';
    else
        $value = strlen($value) . ' bytes';
echo json_encode($data, JSON_PRETTY_PRINT) . "\n";
?>
</pre>
