<?php
require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

require_once __DIR__ . '/../src/crypt.php';
require_once __DIR__ . '/../src/api.php';
require_once __DIR__ . '/../config.php';

use Symfony\Component\HttpFoundation\Request;

$app->get('/api/v1/message/{pubkey}', 'Api\V1\getMessage');
$app->put('/api/v1/message/', 'Api\V1\putMessage');
$app->put('/api/v1/message/{pubkey}', 'Api\V1\putMessage');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    switch($code) {
        case 404:
            $message = 'The requested resource could not be found. Resource: ' . $request->getBasePath();
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }
    return $app->json(['message' => $message], $code);
});

$app->run();
